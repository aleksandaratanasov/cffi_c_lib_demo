#!/usr/bin/python3

from cffi import FFI
import random
from sys import exit, platform

ffi = FFI()
# ffi.cdef('''
#     int printf(const char *format, ...); // copy-pasted from the man page
#     ''')
# C = ffi.dlopen(None)  # Load complete C namespace
# # new() for dynamicall allocated memoryview
# # cast() for (but not only) casting Python primitives to C primitives
# x = 3
# exp = 4
# C.printf(b'Cffi says \"%s\" and also %d^%d=%d\n',
#     ffi.new('char[]', b'Hello cffi!'),
#     ffi.cast('int', x),
#     ffi.cast('int', exp),
#     ffi.cast('int', pow(x, exp)))

cdef_from_file = None
header = 'testcffi.h'
try:
    with open(header, 'r') as libtestcffi_header:
        cdef_from_file = libtestcffi_header.read() #.replace('\n', '')
except FileNotFoundError:
    print('Unable to find "%s"' % header)
    exit(2)
except IOError:
    print('Unable to open "%s"' % header)
    exit(3)
finally:
    if cdef_from_file == '':
        print('File "%s" is empty' % header)
        exit(1)

ffi.cdef(cdef_from_file)
lib_extension = ''
if platform.startswith('freebsd') or platform.startswith('linux'):
    lib_extension = 'so'
elif platform.startswith('win'):
    lib_extension = 'dll'
CLibTC = ffi.dlopen('build/libtestcffi.' + lib_extension)
# d1struct_ptr = ffi.new('point_t *')
# d1struct_ptr = CLibTC.createPoint(1, 2, 3, ffi.new('char[]', 'point_{0}'.format(str(1000)).encode('utf-8')))
# CLibTC.printPoint(d1struct_ptr)

def create_list(length=5):
    if len:
        lst = []
        labels = []
        msg = None
        for i in range(0, length):
            msg = ffi.new('char[]', 'point_{0}'.format(str(i)).encode('utf-8'))
            labels.append(msg) # In order to preserve the label we need to keep it "alive" as long as we need it
            lst.append(CLibTC.createPoint(
                float(random.random()*(i+1)*10),
                float(random.random()*(i+1)*10),
                float(random.random()*(i+1)*10),
                msg
            ))

        return lst, labels
    return None, None


def print_list(lst):
    if lst and len(lst):
        for l in lst:
            CLibTC.printPoint(l)

list_of_dstruct_ptr, list_of_labels = create_list(3)
# list_of_dstruct_ptr =  createList(10000000)
print_list(list_of_dstruct_ptr)

# Access a dynamically allocated point_t struct's parameters
print(list_of_dstruct_ptr[0].x)
print(list_of_dstruct_ptr[0].y)
print(list_of_dstruct_ptr[0].z)
list_of_dstruct_ptr[0].label = ffi.new('char[]', b'foobar')
print(ffi.string(list_of_dstruct_ptr[0].label).decode('utf-8'))
print('#define HELLO ' + str(CLibTC.HELLO))
