#include "testcffi.h"
#include <stdio.h>
#include <malloc.h>

point_t* createPoint(double x, double y, double z, char *label) {
  point_t *p = malloc(sizeof(point_t));
  p->x = x;
  p->y = y;
  p->z = z;
  p->label = label;

  return p;
}

void printPoint(point_t *point) {
  if(point == NULL) return;
  printf("Data:\n\tx : %f\n\ty : %f\n\tz : %f\n\tmsg : \"%s\"\n", point->x, point->y, point->z, point->label);
}

void deletePoint(point_t *point) {
  if(point == NULL) return;
  free(point);
  point = NULL;
}

void addScalar(point_t *point, double scalar) {
  if(point == NULL) return;

  point->x = point->x + scalar;
  point->y = point->y + scalar;
  point->z = point->z + scalar;
}

void subScalar(point_t *point, double scalar) {
  if(point == NULL) return;

  addScalar(point, -scalar);
}

void multScalar(point_t *point, double scalar) {
  if(point == NULL) return;

  point->x = point->x * scalar;
  point->y = point->y * scalar;
  point->z = point->z * scalar;
}

void divScalar(point_t *point, double scalar) {
  if(point == NULL || !scalar) return;

  point->x = point->x / scalar;
  point->y = point->y / scalar;
  point->z = point->z / scalar;
}

void extendPoint(point_t *point, double scalar, char mode) {
  switch(mode) {
    case '+':
      addScalar(point, scalar);
    	break;
    case '-':
      subScalar(point, scalar);
      break;
    case '*':
      multScalar(point, scalar);
      break;
    case '/':
      divScalar(point, scalar);
      break;
  }
}

void rotatePoint(point_t *point, char axis, double deg) {
  
}

void translatePoint(point_t *point, char axis, double dist) {

}