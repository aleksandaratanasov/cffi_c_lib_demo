#include "testcffi.h"
#include <malloc.h>

// The main.c is useful just for testing purposes here (
int main(int argc, char *argv[])
{
  point_t p1 = {.x=10., .y=20., .z=30., "Sofia"};
  printPoint(&p1);
  point_t *p2 = createPoint(1., 2., 3., "Karlsruhe");
  printPoint(p2);
  deletePoint(p2);
  return 0;
}
